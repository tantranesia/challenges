// using forEach to check value of the array if it matched with the position in sequence
// to know the position we used the parameter idx + 1
// if the integer doesn't match the expected position then we need to find the iterger with the right position 
// using indexOf to swap the interger
// swap the matching interger
// add the swap count

const minimumSwaps = (arr) => {
    let count = 0;
    arr.forEach((currentInt, idx) => {
        const position = idx + 1;
        if (currentInt !== position) {
            const idxSwap = arr.indexOf(position);
            arr[idxSwap] = currentInt;
            arr[idx] = position;
            count = count+ 1;
        }
    });
    return count;

}