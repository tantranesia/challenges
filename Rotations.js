const rotLeft = (a, d) => {
    // parameters a is for array to rotate
    // paramater d is for how many rotation will execute
    // while d means how many rotations the funcstion will execute
    // .shift will eliminate the first element inside the array and returns it
    // .push will put the element at the end of array
    // d-- means to prevent an infinite loop, after the first loop d will substract 1, and then zero
    while (d) {
        a.push(a.shift());
        d--;
     }
  return a;
}
